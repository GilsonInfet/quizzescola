import React from 'react'
import { Route, Router, Switch, Redirect } from 'react-router-dom'
import history from './config/history'
import FormLogin from './views/auth/login';

import { getRole, isAuthenticated } from './config/auth';
import Layout from './components/layout/layout';
import LandingHome from './components/landingHome';

import  QuizzView from './views/quizz/viewQuizz';

import UsersView from './views/users/viewAdminUsers';
import TurmaView from './views/turmas/viewTurmas';
import DisciplinaView from './views/disciplinas/viewDisciplina';
import QuestoesView from './views/questoes/viewQuestoes';





/**Rotas específicas da administração.  */
const AdminRoute = ({ ...rest }) => {
    if (!getRole()) {
        return <Redirect to='/login' />
    } else {

        if (!isAuthenticated("ADM")) {
            return <Redirect to={`/${getRole()}`} />
        }

    }
    return <Route {...rest} />
}


/**Rotas específicas dos professores.  */
const ProfessorRoute = ({ ...rest }) => {
    if (!getRole()) {
        return <Redirect to='/login' />
    } else {

        //ADM acessa quaisquer rotas
        if (isAuthenticated("ADM")) {
            return <Route {...rest} />
        }


        if (!isAuthenticated("PROFESSOR")) {
            return <Redirect to={`/${getRole()}`} />
        }

    }
    return <Route {...rest} />
}


/**Rotas específicas dos aluno.  */
const AlunoRoute = ({ ...rest }) => {

            //ADM acessa quaisquer rotas
            // if (isAuthenticated("ADM")) {
            //     return <Route {...rest} />
            // }

    if (!getRole()) {
        return <Redirect to='/login' />
    } else {

        if (!isAuthenticated("ALUNO")) {
            return <Redirect to={`/${getRole()}`} />
        }else{
            console.log("Rota de aluno autorizada", rest)
            return <Route {...rest} />
        }

    }

}

/** Lnks para adm. ADM tem acesso a tudo */
const linksNavegacao = [
    {
        title: "Usuários",
        link: "/adm/caduser",
        component: UsersView
    },
    {
        title: "Turmas",
        link: "/adm/cadturma",
        component: TurmaView
    },
    {
        title: "Disciplina",
        link: "/adm/caddisciplina",
        component: DisciplinaView
    },
    {
        title: "Questoes",
        link: "/adm/cadquestoes",
        component: QuestoesView
    }

]


const linksNavegacaoAluno = [
    {
        title: "Quizz",
        link: "/aluno/quizzaluno",
        component: QuizzView
    }

]

const linksNavegacaoProfessor = [
    {
        title: "Questoes",
        link: "/professor/cadquestoes",
        component: QuestoesView
    }

]
const Routers = () => (


    <Router history={history}>
        <Switch>
            <Route exact component={FormLogin} path="/login" />
            <Route exact component={FormLogin} path="/" />
            <Route path="/adm">
                <Layout linksNavegacao={linksNavegacao}>
                    <AdminRoute exact path="/adm" component={LandingHome} />
                    {linksNavegacao.map((item, index) =>
                        <AdminRoute key={index} exact path={item.link} component={item.component} />
                    )}

                </Layout>
            </Route>

            <Route path="/professor">
                <Layout linksNavegacao={linksNavegacaoProfessor}>
                    <ProfessorRoute exact path="/professor" component={LandingHome} />
                    {linksNavegacaoProfessor.map((item, index) =>
                        <ProfessorRoute key={index} exact path={item.link} component={item.component} />
                    )}
                </Layout>
            </Route>

            <Route path="/aluno">
                <Layout linksNavegacao={linksNavegacaoAluno}>
                    <AlunoRoute exact path="/aluno" component={LandingHome} />
                    {linksNavegacaoAluno.map((item, index) =>
                        <AlunoRoute key={index} exact path={item.link} component={item.component} />
                    )}

                </Layout>
            </Route>

        </Switch>
    </Router>
)

export default Routers;
