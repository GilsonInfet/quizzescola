
import styled from "styled-components";
import Swal from "sweetalert2";
import React from 'react'

export const bases3 = process.env.S3_BASENAME || "https://quizzgpweb.s3-sa-east-1.amazonaws.com/" //"https://quizzgpweb.s3-sa-east-1.amazonaws.com/"
// const rootbasename = process.env.S3_BASENAME 

/**
 *type danger /info / sucess 
 *message mensagem da desejada
 */
export const message = (type, message) => {
    Swal.fire({
        position: 'center',
        icon: type,
        title: message,
        showConfirmButton: false,
        timer: 2500
    })
}

/** 
 * Uso:
 *  swalConfirmation(`TUA MENSAGEM`, 
 * ()=>{FUNÇÃO A SER EXECUTADA EM CASO DE SIM  })}  
 *  */
export const swalConfirmation = async (mensagem, callback) => {

    const resultConfirmation = await Swal.fire({
        title: `${mensagem}`,
        showCancelButton: true,
        confirmButtonText: `Sim`,
        cancelButtonText: `Não`
    })

    if (resultConfirmation.isConfirmed) {
        callback()
    }
    return

}


export const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: scroll;    
    padding: 20px;
`



/**A props URL deve terminar com o código do vídeo contendo 11 caracteres*/
export function ResponsiveIframe(props) {

    var embebed = props.url.substr(props.url.length - 11);
    return (
        <ResponsiveVideoDiv>

            <div className="container">
                <iframe
                    className="responsive-iframe"
                    width="560" height="315"
                    src={`https://www.youtube.com/embed/${embebed}`} 
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen>
                </iframe>
            </div>
        </ResponsiveVideoDiv>
    )
}


const ResponsiveVideoDiv = styled.div`
.container {
    position: relative;
    overflow: hidden;
    width: 100%;
     padding-top: 56.25%; 
  }
  
  /* Then style the iframe to fit in the container div with full height and width */
  .responsive-iframe {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100%;
  }
`