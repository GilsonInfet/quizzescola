import http from '../config/http'


/**
 *rota nome simples da rota
 filter é um string simples, exemplo :  highlight=true
 */
const getWhatIWant = (rota, filter="" ) => {
    return !filter ?  http.get(`/${rota}`) : http.get(`/${rota}?${filter}`)
}



//USER
const createUser  = (data, config = {}) => http.post(`/user`, data, config)
const updateUser = (id, data) => http.patch(`/user/${id}`, data)
const deleteUser = (id) => http.delete(`/user/${id}`)



//TURMA
/**data é o objeto que cria turma deve ter number e yarcalendar */
const createTurma = (data) => http.post(`/turma`, data)
/**Qyery é o objeto que irá filtrar a busca de turmas */
const getTurma = (query) => http.get('/turma',{ params : query })
/**Deleta uma turma por id mongo */
const deleteTurma = (id) => http.delete(`/turma/${id}`)
/**Update turma */
const updateTurma = (id, data) => http.patch(`/turma/${id}`, data)



//Vou fazer patch??? precisa??

//TURMA
/**data é o objeto que cria turma deve ter number e yarcalendar */
const createDisciplina = (data) => http.post(`/disciplina`, data)
/**Qyery é o objeto que irá filtrar a busca de turmas */
const getDisciplina = (query) => http.get('/disciplina',{ params : query })
/**Deleta uma turma por id mongo */
const deleteDisciplina = (id) => http.delete(`/disciplina/${id}`)
const updateDisciplina = (id, data) => http.patch(`/disciplina/${id}`, data)


//PERGUNTA:
const createPergunta  = (data, config = {}) => http.post(`/pergunta`, data, config)
const updatePergunta = (id, data) => http.patch(`/pergunta/${id}`, data)
const deletePergunta = (id) => http.delete(`/pergunta/${id}`)
const getPergunta = (query) => http.get('/pergunta',{ params : query })


//CONFERE RESPOSTA:
/**Mandar no data um objeto { resposta : "String_da_resposta" } . id é o _id mong da pergunta*/
const confirmaRespostaID = (id, data) => http.post(`/confirmacao/${id}`, data)



export {
createPergunta, updatePergunta, deletePergunta, getPergunta,
updateTurma,
updateDisciplina,
confirmaRespostaID,
getWhatIWant,
createUser,
deleteUser,
updateUser,
createTurma,
deleteTurma,
getTurma,
createDisciplina,
deleteDisciplina,
getDisciplina
}
