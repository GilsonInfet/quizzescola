import http from '../config/http'

/**requisição para rota de login */
const authentication = (data) => http.post('/auth', data)

export {
    authentication
}
