import React, { useState } from 'react';
import { Form, Button, Container, Spinner, Col } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { saveToken } from '../../config/auth';
import { authentication } from '../../services/auth';
import http from '../../config/http'

import history from '../../config/history'
import styled from 'styled-components';
import loginBgi from '../../assets/images/loginbgi.JPG'


function FormLogin() {


    const [form, setForm] = useState({

    })
    const [loading, setLoading] = useState(false)

    const handleChange = (attr) => {
        setForm({
            ...form,
            [attr.target.name]: attr.target.value
        })
    }

    const isSubmitValid = () => form.email && form.password


    const submitLogin = async (e) => {
        e.preventDefault()

        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })

        if (isSubmitValid()) {
            setLoading(true)
            try {
                const { data } = await authentication(form)

                const { token } = data
                http.defaults.headers["x-auth-token"] = token;
                saveToken(data)
                history.push('/adm')

            } catch (error) {
                setLoading(false)
                console.log(error)
            }
        }
    }






    return (
<Bglayimage>
        <ContainerStyled>

            <ColStyled>
                <Col xs={12} sm={12} md={6} lg={4} xl={4}  >
                    <FormStyled>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control onChange={handleChange} name="email" type="email" placeholder="Enter email" value={form.email || ""} />
                            <Form.Text className="text-muted">
                                Teu email de cadastro.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Senha</Form.Label>
                            <Form.Control onChange={handleChange} name="password" type="password" placeholder="Senha" value={form.password || ""} />
                        </Form.Group>

                        <Button block disabled={!isSubmitValid()} onClick={submitLogin} variant="primary" type="submit"> 
                        {loading ? <Spinner animation="grow" role="status" /> : "Enviar"}
                          
                        </Button>
                        
                    </FormStyled>
                </Col>
            </ColStyled>
        </ContainerStyled>
        </Bglayimage>
    )
}


export default FormLogin;

const ContainerStyled = styled(Container)`
 display: flex;
align-items: center;
justify-content :center;  

`

const FormStyled= styled(Form)`

background-color: rgba(0,0,0,0.7);
padding: 10px;
border-radius: 5px;
color:white;
`


const Bglayimage = styled.body`
        background-image: url(${loginBgi}) ; 
        background-color: #cccccc; /* Used if the image is unavailable */
        height: 100vh; /* You must set a specified height */
        background-attachment: fixed;
        background-position: center;
        background-repeat: repeat;
        background-size: cover;
        `

const ColStyled = styled(Col)`
 display: flex;
align-items: center;
justify-content :center;
height:80vh;
`