import React, { useEffect, useState } from 'react'
import { Button, Container  } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import UsersForm from '../../components/admin/formUser'
import ListUsers from '../../components/admin/listUser'


const UsersView = () => {

    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})
    const location = useLocation();


       /** recebe o user a ser atualizado. Chama exibição do form de user*/
    const updateUser = (prd) => {

        setUpdate(prd)             
        setForm(true)
    }


    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }

    }, [location])


const toggleUpdateCreate = ()=>{

    setForm(!isForm)
    setUpdate({})
}

     return (
        <div className="alturaminima">

<br/>
        <Container>
            <Button size="sm" variant="success" onClick={() => toggleUpdateCreate()}>
                {isForm ? "Lista de Usuários" : "Novo usuário"}
            </Button>
          
        
            { isForm
                ? <UsersForm update={update} />
                : <ListUsers updateUser={updateUser} />
            }

</Container>
          
        </div>
    )
}

export default UsersView



