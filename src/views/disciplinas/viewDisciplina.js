import React, { useEffect, useState } from 'react'
import { Button, Container } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import FormDisciplina from '../../components/admin/formDisciplina'
import ListDisciplina from '../../components/admin/listDisciplina'


const DisciplinaView = () => {

    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})
    const location = useLocation();


    /** recebe o user a ser atualizado. Chama exibição do form de user*/
    const updateDisciplina = (prd) => {

        setUpdate(prd)
        setForm(true)
    }


    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }

    }, [location])


    return (
        <>

            <br />
            <Container>
                <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                    {isForm ? "Lista de Disciplinas" : "Nova Disciplina"}
                </Button>
            

            { isForm
                ? <FormDisciplina update={update} />
                : <ListDisciplina updateUser={updateDisciplina} />
            }
</Container>

        </>
    )
}

export default DisciplinaView



