import React, { useEffect, useState } from 'react'
import { Button, Container } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import ListQuizz from '../../components/aluno/listQuizz'
import QuizzAluno from '../../components/aluno/formQuizz'



const QuizzView = () => {

    const [isForm, setForm] = useState(false)
    const [result, setResult] = useState([])
    const location = useLocation();


    /** recebe o user a ser atualizado. Chama exibição do form de user*/
    const exibeResultsList = (prd) => {
console.log("exibeResultsList Chamada")
        setResult(prd)
        setForm(false)
    }


    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }

    }, [location])


    return (
        <>

            <br />
            <Container>
                <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                    {isForm ? "Resultados" : "Nova Quizz"}
                </Button>
            

            { isForm
                ? <QuizzAluno listResults={exibeResultsList} />
                : <ListQuizz results={result} />
            }
            </Container>
        </>
    )
}

export default QuizzView



