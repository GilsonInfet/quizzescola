import React, { useEffect, useState } from 'react'
import { Button, Container  } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import FormTurma from '../../components/admin/formTurma'
import ListTurmas from '../../components/admin/listTurma'


const TurmaView = () => {

    const [isForm, setForm] = useState(true)
    const [update, setUpdate] = useState({})
    const location = useLocation();

        const simpleToggleListForm = () => {
            setUpdate({}) 
            setForm(!isForm)

        }

    /** recebe a turma a ser atualizado. Chama exibição do form de turma*/
    const updateTurma = (prd) => {
        setUpdate(prd)             
        setForm(true)
    }


    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }

    }, [location])

//5fe27c2a4cbd9c0bb02b5042
     return (
        <>

        <br/>
        <Container>
            <Button size="sm" variant="success" onClick={() => simpleToggleListForm(!isForm)}>
                {isForm ? "Lista de Turmas" : "Nova Turma"}
            </Button>
            
        
            { isForm
                ? <FormTurma update={update} />
                : <ListTurmas updateUser={updateTurma} />
            }

</Container>
          
        </>
    )
}

export default TurmaView



