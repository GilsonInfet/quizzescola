import React, { useEffect, useState } from 'react'
import { Button, Container } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import FormQuestoes from '../../components/professor/formQuestoes'
import ListQuestoes from '../../components/professor/listQuestoes'


const QuestoesView = () => {

    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})
    const location = useLocation();


    /** recebe o user a ser atualizado. Chama exibição do form de user*/
    const updateQuestoes = (prd) => {

        setUpdate(prd)
        setForm(true)
    }


    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }

    }, [location])


    return (
        <div className="alturaminima">

            <br />
            <Container>
                <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                    {isForm ? "Lista de Questões" : "Nova Questão"}
                </Button>
           
                <br />
            { isForm
                ? <FormQuestoes update={update} />
                : <ListQuestoes updateUser={updateQuestoes} />
            } 
            </Container>
        </div >
    )
}

export default QuestoesView



