
import React, { useState } from 'react'
import { Form, Image, Nav, Navbar, NavDropdown, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import { getUser, removeToken } from '../../config/auth';
import history from '../../config/history';
import semfoto  from '../../assets/images/semphoto.png'
import { bases3 } from '../../util/index'

const Layout = ({ children, linksNavegacao }) => {
    const [expanded, setExpanded] = useState(false);
    const userLogado = getUser()

    const sair = () => {
        removeToken()
        history.push("/login")
    }


      const userphoto = userLogado.photo ? `${bases3}${userLogado.photo}` : semfoto
   
    return (
        <>
            {/* <Container> */}
                <Navbar bg="dark" variant="dark">



                    <NavDropdown className="mr-auto" title="Menu Nav" id="nav-dropdown">
                        <NavLink exact={true} to={"/adm"} >
                            <Nav.Link as="div" > HOME</Nav.Link>
                        </NavLink>
                        {linksNavegacao.map((item, i) => (
                            <NavLink exact={true} to={item.link} key={i}>
                                <Nav.Link onClick={() => setExpanded(false)} as="div" > {item.title}</Nav.Link>
                            </NavLink>
                        ))}
                    </NavDropdown>

                    <Form inline>





                        <>
                        <div style={{color:'white', 'margin-right':'5px'}}> {`${userLogado.orgname} - ${userLogado.name} - ${userLogado.role}`}</div>
                            {['bottom'].map((placement) => (
                                <OverlayTrigger
                                    key={placement}
                                    placement={placement}
                                    overlay={
                                        <Tooltip id={`tooltip-${placement}`}>
                                           {`${userLogado.name} - Clique para Sair`}
                                        </Tooltip>
                                    }
                                >
                                   <ImageStyled  onClick={() => sair()} src={userphoto} roundedCircle />
                                </OverlayTrigger>
                            ))}
                        </>

                    </Form>

                </Navbar>

                {children}


            {/* </Container> */}
            <StyledFooter>

                    <p>{`Site desenvolvido por ` }<a href="https://gilsonpaulo.com.br/"> GPWEB</a></p>
               

            </StyledFooter>


        </>
    )
}

export default Layout




const StyledFooter = styled.footer`
background-color:rgb(52, 58, 64);
color:whitesmoke;
padding: 10px;
height:200px;
display:flex;
justify-content:center;
align-items:center;

`
const ImageStyled = styled(Image)`
height:50px;
width:50px;
cursor: pointer;
`
