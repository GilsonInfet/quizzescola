import React, { useState, useEffect, useRef } from 'react'
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap'
import styled from 'styled-components'
import {  getUser } from '../../config/auth'
import { useDispatch, useSelector } from 'react-redux'
import { disciplinasListAll } from '../../store/disciplinas/disciplinas.actions'
import { bases3, message, ResponsiveIframe, swalConfirmation } from '../../util/index'
import { perguntasListAll } from '../../store/perguntas/perguntas.actions'
import { Prompt } from 'react-router-dom'

const QuizzAluno = (props) => {
    //O load não é imediato mesmo já tendo questões carregadas na store. Esse setstate evita o erro de tenbtar render uma 
    //pergunta que ainda não carregou.

    const perguntaDummy = {
        _perguntaEmTela: "perguntaEmTela",
        pergunta: "MOck ",
        opcoes: ["e3", "e2", "e1", "c3", "e4"],
        disciplina: "matematica",
        ano: "9",
        photo: "pergunta/5fdf4e69ecf8b11a2894a64d-5fe2552b079a1f7ca00d6ba7-fone.png",
        video: "https://youtu.be/014fy8sGPto",
        cadastradoPor: "Wilson",
        instituicao: "c123456",
        opcaoEscolhida: ""
    }

    const [perguntaEmTela, setPerguntaEmTela] = useState(perguntaDummy)

  
    /**Informa se há um quizz iniciado /em andamento*/
    const [iniciado, setIniciado] = useState(false)

    /**objeto que filtra as perguntas que serão usadas no quizz */
    const [formFilter, setFilter] = useState({})
    const perguntas_redux = useSelector(state => state.perguntas.perguntasAll)
    const disciplinas_redux = useSelector(state => state.disciplinas.disciplinasAll)

    /**Lista das perguntas que estão sendo usada no quiz */
    const [filtredList, setfiltredList] = useState([])
    const [numeroQ, setnumeroQ] = useState(-1)
    const qtdQuizEscolhida = useRef(null);

    const radio0 = useRef(null);
    const radio1 = useRef(null);
    const radio2 = useRef(null);
    const radio3 = useRef(null);
    const radio4 = useRef(null);
    const org = getUser()

    const dispatch = useDispatch()
    
    //Carrega a lista de todas as questões existentes para esta org
    useEffect(() => {
        dispatch(perguntasListAll())
    }, [dispatch])

    
//Carrega a lista de todas as disciplinas existentes para esta org
    useEffect(() => {
        dispatch(disciplinasListAll({ organizacao: org.organizacao }))
    }, [dispatch])


    //Reset radios está aqui pois dá erro se colocar direto no botão de trocar questão.
    //usestate não é imediato
    useEffect(() => {
        restartRadios()
    }, [perguntaEmTela])


    //Este é para atualizar a pergunta em tela selecionada de filtredList
    useEffect(() => {
        if (filtredList.length > 0) {
            setPerguntaEmTela(
                {
                    ...filtredList[numeroQ],
                    _perguntaEmTela: "perguntaEmTela"
                })
        }
    }, [numeroQ])


    /**Muda a questão em tela */
    const changequestion = (x) => {
        setnumeroQ(numeroQ + 1 * x)
    }

    /**Chama Multifilter em perguntas gerais :: multiFilter(perguntas_redux)*/
    const refreshFilter = () => {
        const x = multiFilter(perguntas_redux)
    }

    /**Faz a lista filtrada ser redefinida conforme formFilter */
    const multiFilter = (arr, iformFilter = formFilter) => {
        //Embaralha as perguntas do array passado "perguntas_redux que é = todas as disponíveis de todas as disciplinas " 
        let tempArr = [...arr].sort(() => Math.random() - 0.5);


        //Para o QuizzAluno só me interessa a disciplina
        for (let [keyFilter, value] of Object.entries(iformFilter)) {
            if (keyFilter === "disciplina") {
                tempArr = tempArr.filter(item => item[keyFilter]._id === value)
            }
        }

        if (!formFilter.quantidade) {
            message('info', `Defina uma quantidade de até ${tempArr.length} questões`)
            return
        }

        if (formFilter.quantidade && formFilter.quantidade > tempArr.length) {
            message('info', `O máximo de questões disponíveis para esta disciplina é ${tempArr.length}`)
            return
        }

        tempArr = tempArr.slice(0, formFilter.quantidade)
        setfiltredList(tempArr)

        message('info', `${tempArr.length} questões disponíveis ou carregadas`)
        // setTimeout(() => { iniciar() }, 2500)
        
    }

    const substituiEmArray = (arrOriginal, removerIndex, insereEste, setStateFunction, qtd = 1) => {
        //Copia array original
        let tempArr = [...arrOriginal]
        //Remove umm item e insere outro
        let removido = tempArr.splice(removerIndex, qtd, insereEste);
        setStateFunction(tempArr)
    }


    /**Check ou uncheck os radiosbuttons das respostas conforme respostas escolhidas */
    const restartRadios = () => {
        const radios = [radio0, radio1, radio2, radio3, radio4]
        try {
            radios.map(item => item.current.checked = false)
            radios.map((item) => {
                if (perguntaEmTela.opcaoEscolhida && item.current.value === perguntaEmTela.opcaoEscolhida) {
                    item.current.checked = true
                }
            })
        } catch (error) {

        }
    }




    /**Handlechange para io filtyer de disciplina apenas */
    const handleChangeFilter = (attr) => {
        const { value, name } = attr.target

        setFilter({
            ...formFilter,
            organizacao: org.organizacao,
            me: "formFilter",
            [name]: value
        })

        return;
    }


    /**Este é o handle das opções marcadas.*/
    const handleChange = (attr) => {
        const { value, name } = attr.target

        var cloneArray = [...filtredList]

        const pos = cloneArray.map(function (e) { return e._id; }).indexOf(perguntaEmTela._id);

        const cloneTemPergunta = {
            ...cloneArray[pos],
            opcaoEscolhida: value
        }

        setPerguntaEmTela({
            ...cloneTemPergunta,
            opcaoEscolhida: value,
            _perguntaEmTela: cloneTemPergunta._id
        })

        substituiEmArray(filtredList, pos, cloneTemPergunta, setfiltredList)
        return;
    }


    /**Inicia o teste,  */
    const iniciar = () => {
        changequestion(1)
        setIniciado(true)
    }

    /** Faz changequestion(-1) setIniciado(false) e setfiltredList([])*/
    const abortarTeste = () => {

        swalConfirmation(`Deseja mesmo abortar o Quizz?`, 
        ()=>{ 
            setnumeroQ(-1)
            setIniciado(false)
            setfiltredList([])
            /**Há um atraso para setar a pergunta em tela por isso coloco uma pergunta dummy.
             *  a pergunta 0 do array tem que ter algum dado*/
            setPerguntaEmTela(perguntaDummy)        
        }) 

    }

    const confirmaQuestao = async () => {
        

        if (filtredList.some(e => !e.opcaoEscolhida)) {
            message('danger', "Não concluído")
            return
        }

        // filtredList.forEach(async (element) => {
        //     await confirmaRespostaID(element._id, { resposta: element.opcaoEscolhida })
        //         .then(res => {
        //             console.log(res.data)
        //             element.acerto = res.data.acerto
        //         })
        //         .catch(err => console.log(err))
        // });

        // debugger

props.listResults(filtredList)

    }

    return (
        < >
            <Prompt
                when={iniciado}
                message='O Quizz não foi concluído. As respostas serão perdidas. Quer sair mesmo assim?'
            />

            <Anim>
            <h4>Quizz Aluno:</h4>
            </Anim>
            <br />

            <Row>
                <Col sm="7">
                    <Form.Group >

                        {!disciplinas_redux ? <Spinner animation="grow" role="status" /> :
                            <Form.Control as="select" custom name="disciplina" onChange={handleChangeFilter} value={formFilter.disciplina || ""}>
                                <option value=""> --DISCIPLINA-- </option>

                                {disciplinas_redux.map((disciplina, i) => (
                                    <option key={i} value={disciplina._id}> {disciplina.name} - Ano: {disciplina.yearstage} Ano Calendar: - {disciplina.yearcalendar}  </option>
                                ))}
                            </Form.Control>}

                    </Form.Group>
                </Col>

                <Col>
                    <Form.Group >

                        <Form.Control ref={qtdQuizEscolhida} type="number" onChange={handleChangeFilter} name="quantidade" value={formFilter.quantidade || ""}
                            placeholder={`Máx ${filtredList.length}`} />

                    </Form.Group>
                </Col>

                <Col>
                    <Button disabled={!formFilter.quantidade || !formFilter.disciplina || iniciado} block variant="primary" onClick={refreshFilter}>  Carregar </Button>
                </Col>


            </Row>

            <Button disabled={iniciado || filtredList.length <= 0} block variant="dark" onClick={iniciar}>  Iniciar </Button>

            <br />

            {!iniciado ? <h4>Selecione uma disciplina e uma quantidade de questões, depois clique em iniciar. </h4> :
                <>

                    <h4>{`Questão ${numeroQ + 1} de  ${filtredList.length} : `} </h4>
                    <br />
                    <Form.Group >
                        <Form.Control autocomplete="off" type="text" as="textarea" rows={3} name="pergunta" value={perguntaEmTela.pergunta || ""}
                            readOnly placeholder="pergunta / Enunciado" />
                    </Form.Group>


                    { perguntaEmTela.photo ? <ImgResponsive src={bases3 + perguntaEmTela.photo}
                        alt="temp" /> : ""}


                    {perguntaEmTela.video ? <ResponsiveIframe url={perguntaEmTela.video} /> : ""}


                    <br />

                    <fieldset>
                        <Pstyled>
                            <Form.Group as={Row}>
                                <Col sm={12}>
                                    <Form.Check
                                        type="radio"
                                        label={<p>{perguntaEmTela?.opcoes[0]}</p>}
                                        name="formHorizontalRadios"
                                        id="formHorizontalRadios1"
                                        value={perguntaEmTela?.opcoes[0]}
                                        onChange={handleChange}
                                        ref={radio0}
                                    // checked={radio0.current?.value === filtredList[numeroQ].opcaoEscolhida || false}
                                    />
                                    <Form.Check
                                        type="radio"
                                        label={<p>{perguntaEmTela.opcoes[1]}</p>}
                                        name="formHorizontalRadios"
                                        id="formHorizontalRadios2"
                                        value={perguntaEmTela?.opcoes[1]}
                                        onChange={handleChange}
                                        ref={radio1}
                                    // checked={radio1.current?.value === filtredList[numeroQ].opcaoEscolhida || false}
                                    />
                                    <Form.Check
                                        type="radio"
                                        label={<p>{perguntaEmTela.opcoes[2]}</p>}
                                        name="formHorizontalRadios"
                                        id="formHorizontalRadios3"
                                        value={perguntaEmTela?.opcoes[2]}
                                        onChange={handleChange}
                                        ref={radio2}
                                    // checked={radio2.current?.value === filtredList[numeroQ].opcaoEscolhida || false}
                                    />
                                    <Form.Check
                                        type="radio"
                                        label={<p>{perguntaEmTela.opcoes[3]}</p>}
                                        name="formHorizontalRadios"
                                        id="formHorizontalRadios4"
                                        value={perguntaEmTela?.opcoes[3]}
                                        onChange={handleChange}
                                        ref={radio3}
                                    // checked={radio3.current?.value === filtredList[numeroQ].opcaoEscolhida || false}
                                    />

                                    <Form.Check
                                        type="radio"
                                        label={<p>{perguntaEmTela.opcoes[4]}</p>}
                                        name="formHorizontalRadios"
                                        id="formHorizontalRadios5"
                                        value={perguntaEmTela?.opcoes[4]}
                                        onChange={handleChange}
                                        ref={radio4}
                                    // checked={radio4.current?.value === filtredList[numeroQ].opcaoEscolhida || false}
                                    />
                                </Col>
                            </Form.Group>
                        </Pstyled>
                    </fieldset>

                    <Row>
                        <Col sm={4}>
                            <Button disabled={(numeroQ + 1) === 1} block variant="dark" onClick={() => changequestion(-1)}>  ANTERIOR </Button>
                        </Col>


                        <Col sm={4}>
                            <Button block disabled variant="outline-dark">  {`${numeroQ + 1}/${filtredList.length} `} </Button>
                        </Col>

                        <Col sm={4}>
                            <Button disabled={(numeroQ + 1) === filtredList.length} block variant="dark" onClick={() => changequestion(1)}> PRÓXIMA </Button>
                        </Col>
                    </Row>
                </>
            }
            <br />
            {iniciado ? <Button disabled={!iniciado} block variant="dark" onClick={abortarTeste}>Abortar Quizz </Button> : ""}
            <br /><br /><br />

            <Anim>
            {iniciado ? <Button  disabled={!iniciado || filtredList.some(e => !e.opcaoEscolhida)} 

            block variant="outline-info" onClick={confirmaQuestao}>Submeter Quizz </Button> : ""}
</Anim>
        </>



    )



}




export default QuizzAluno

const Pstyled = styled.p`
width: 100%;


label {
    width: 100%;
    margin-bottom: 10px;
    border: solid 2px  rgb(206, 212, 218);   
    border-radius:5px;
    vertical-align:center;
    min-height: 50px;
}

`

const ImgResponsive = styled.img`
margin-top: 1px;
margin-bottom: 10px;
  width: 100%;
  height: auto;
  border: solid 1px  rgb(206, 212, 218);
`


const Anim = styled.div`
.aclass {
  animation-duration: 1s;
  animation-iteration-count: infinite;
  animation-name: slidein;
}

@keyframes slidein {

  0% { 
 background-color: rgb(52, 58, 64);
}

  50% {   
    background-color: rgb(23, 162, 184);  
}

  100% { 
    background-color: rgb(52, 58, 64);  
}

}
`