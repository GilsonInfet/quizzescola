import React, { useState, useEffect } from 'react'
import {  Table, Container } from 'react-bootstrap'
import { FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import styled from 'styled-components'
import { bases3 } from '../../util/index'
import Swal from 'sweetalert2'
import { confirmaRespostaID } from '../../services/admin'

/**Userlist do painel do admin */
const ListQuizz = (props) => {

    const [reloadlist, setreloadlist] = useState(0)

    const [results, setResults] = useState(props.results)
   


    
    const confirmaQuestao = async () => {
        

        results.forEach(async (element) => {
            await confirmaRespostaID(element._id, { resposta: element.opcaoEscolhida })
                .then(res => {
                    
                    element.acerto = res.data.acerto
                    setreloadlist(reloadlist + 1)
                    console.log(res.data)
                })
                .catch(err => console.log(err))
        });
    }

    useEffect(() => {
        console.log("props.results[0].acerto", reloadlist)
        // setResults(props.results)

        
        // setTimeout((e) => setreloadlist(reloadlist + 1), 2000) 
    }, [reloadlist])


    useEffect(() => {
        confirmaQuestao()

    }, [])









    const _detalhe = (questao) => {

        Swal.fire({

            title: questao._id,

            imageUrl: bases3 + questao.photo,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            html:
                `Enunciado : ${JSON.stringify(questao)} <br/>`
        })
    }



    const arrStrToList = (exer) => {

        const mont = () => exer.map((item, index) => (
            <li key={index}>{item}</li>
        ))

        return (
            <UlStyled >
                {mont()}
            </UlStyled>
        )

    }

    // (results.length <= 0 && reloadlist > 0 )

    return (
        <>
            <br />
            {results.length > 0 ? <h4>{`${results.filter(a=> a.acerto ).length} acerto(s) dentre ${results.length}  questões`} </h4> : ""}
            <br />
            <Container>



            </Container>
            <Scrolable>
                <Table striped hover  >
                    <thead>
                        <tr>
                            <THeadItem>Disciplina</THeadItem>
                            <THeadItem>Enunciado</THeadItem>
                            <THeadItem>Imagem</THeadItem>
                            <THeadItem>Video</THeadItem>
                            <THeadItem>Opções</THeadItem>
                            <THeadItem>Opção Escolhida</THeadItem>
                            <THeadItem>Resultado</THeadItem>


                        </tr>
                    </thead>
                    <tbody>

                        {(reloadlist <= 0 ) ? <h4>{`Listando resultados de ${results.length} questões`} </h4> :
                            
                            <>
                                {results.map((pergunta, i) => (

                                   
                                    <tr key={i}>
                                        <td>{`${pergunta.disciplina.yearcalendar} - ${pergunta.disciplina.yearstage} - ${pergunta.disciplina.name}`}</td>
                                        <td>{pergunta.pergunta}</td>
                                        <td>{pergunta.photo ? <Responsive onClick={() => _detalhe(pergunta)} src={bases3 + pergunta.photo} alt="Imagem da pergunta" /> : "N/A"}</td>
                                        <td>{pergunta.video ? <a href={pergunta.video} target='_blank'>Link Video</a> : "N/A"}</td>
                                        <td>{arrStrToList(pergunta.opcoes)}</td>
                                        <td>{pergunta.opcaoEscolhida}</td>
                                        <td>{pergunta.acerto === true ? <FaCheckCirclegreen />: <FaTimesCirclered />}</td>

                                    </tr>
                                ))}
                            </>
                        }

                    </tbody>
                </Table>
            </Scrolable>
        </>
    )
}



export default ListQuizz

const UlStyled = styled.ul`
	/* list-style: none;  */
	margin-left: 0;
	padding-left: 30px;
    text-align:left;
`

const Responsive = styled.img`
width: auto;
height: 60px;
margin:3px;

`


const Scrolable = styled.div` 
resize:both;                   
    min-width: 50%;
    max-width: 100%;
    overflow:auto;
    height: auto;
    /* overflow-x: auto;
    overflow-y: auto;     */
    padding: 0px;

border: 1px solid rgba(102, 102, 102, 0.16);
    table{
        tbody{
            tr{
                td{
                    text-align:center;
                    border: 1px solid white;
                    /* vertical-align:sub; */
                }
            }
        }
    }
`
const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;

    :nth-child(1){  width: 15%; } 
    :nth-child(2){  width: 15%; } 
    :nth-child(3){  width: 15%; } 
    :nth-child(4){  width: 15%; } 
    :nth-child(5){  width: 15%; } 
    :nth-child(6){  width: 15%; } 
    :nth-child(7){  width: 15%; } 

`



const FaCheckCirclegreen = styled(FaCheckCircle)`
color:green;
`

const FaTimesCirclered = styled(FaTimesCircle)`
color:red;
`
