import React, { useState, useEffect, useRef } from 'react'
import { Button,  Form, ProgressBar, Spinner, Col, Row } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { createUser, updateUser } from '../../services/admin'
import styled from 'styled-components'
import { getOrg } from '../../config/auth'
import { useDispatch, useSelector } from 'react-redux'
import { turmasListAll } from '../../store/turmas/turmas.actions'
import { FaFileImport } from 'react-icons/fa'

const UsersForm = (props) => {
    const picture = <FaFileImport/>
    const inputEl = useRef(null)
    const org = getOrg()
    const [previewPhoto, setPrewiewPhoto] = useState(null)


    
    const [formUser, setFormUser] = useState({
        ...props.update,
         turma : props?.update?.turma?._id || "",
            organizacao : getOrg()        
        })



    const [progress, setProgress] = useState(0)
    const [updatePhoto, setUpdatePhoto] = useState(false)
    const [isProcessando, setisProcessando] = useState(false)
    const turmas_redux = useSelector(state => state.turmas.turmasAll)
    
   
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(turmasListAll())
    }, [dispatch])

    useEffect(() => {

    }, [progress])

    const isUpdate = Object.keys(props.update).length > 0

    /**isUpdate ? updateUser(props.update._id, data) : createUser(data) */
    const typeReq = (data, config) => isUpdate ? updateUser(props.update._id, data, config) : createUser(data, config)
    const clickopenfile = ()=>{       inputEl.current.click()   }

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show'

        if (name === 'photo') {
            
            try {

                setPrewiewPhoto(URL.createObjectURL(attr.target.files[0]) || null)
            
            } catch (error) {
                console.log(error)
                setPrewiewPhoto(null)
            }

            setFormUser({
                ...formUser,
                'photo': attr.target.files[0]
            })
        } else {

            setFormUser({
                ...formUser,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    const submitUser = async () => {
        //debugger
        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })

        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(formUser)
            .forEach(key => data.append(key, formUser[key]))

        if (typeof formUser.photo === "string") {
            //se photo é != de formdata é update (patch) que não trocou a foto. Tem que ser removido do formdata
            data.delete('photo')
        } 

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        typeReq(data, config)

        .then((res) => {
                clearForm()
                message('success', `Usuário Cadastrado com sucesso.`)
                setisProcessando(false)

            })
            .catch((err) => {
                message('error', `Erro ao cadastrar usuário.${err.message}`)
                clearForm()
            })
        setisProcessando(false)

    }

    const clearForm = () => {
        setProgress(0)
        setUpdatePhoto(true)
        setFormUser({})
    }

    const isNotValid = () => {
        return Object.keys(formUser).some(k => typeof formUser[k] === "string" && formUser[k] === "") || (!formUser.photo)
    }

    const emailNotValid = (email) => {

        if (typeof (email) == 'undefined') {
            return false
        }
        if (email !== "") {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(email)) {
                return true
            } else {
                return false
            }
        }
    }

    const removePhoto = () => {
        // setUpdatePhoto(true)
        setPrewiewPhoto("")
        setFormUser({
            ...formUser,
            photo: ""
        })
        
    }

    return (
        <div className="alturaminima">           

            <br />
                <h4>Cadastro de Usuários:</h4>
                <br />

                <Form>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={8} xl={6}>
                <Form.Group >
                    <Form.Control autocomplete="off" type="text" onChange={handleChange} name="nome" value={formUser.nome || ""}
                        placeholder="Nome do Usuário" />
                </Form.Group>

                <Form.Group >
                <Form.Control as="select" custom name="tipo" onChange={handleChange} value={formUser.tipo}>
                    <option value="">----TIPO----</option>
                    <option value="ADM">ADM</option>
                    <option value="ALUNO">ALUNO</option>
                    <option value="PROFESSOR">PROFESSOR</option>
                </Form.Control>
            </Form.Group>

                <Form.Group >
                    <Form.Control autocomplete="off" type="text" autocomplete="off" onChange={handleChange} name="serie" value={formUser.serie || ""}
                        placeholder="Série" />
                </Form.Group>


                <Form.Group >
                <Form.Control as="select" custom name="turma" onChange={handleChange} value={formUser.turma}>
                    <option value="">--TURMA----</option>
                         {turmas_redux.map((turma, i) => (
        				<option key={i} value={turma._id}> {`${turma.yearcalendar} - ${turma.number} `} </option>
                   
                         ))}
                </Form.Control>
            </Form.Group>

                <Form.Group >
                    <Form.Control autocomplete="off" isInvalid={emailNotValid(formUser.email)} type="email" onChange={handleChange} name="email" value={formUser.email || ""}
                        placeholder="Email do Usuário" />
                </Form.Group>

                <Form.Group >
                    <Form.Control autocomplete="false" type="password"  autocomplete="off" onChange={handleChange} name="password" value={formUser.password || ""}
                        placeholder="Senha do Usuário" />
                </Form.Group>


                <Form.Group >
                <Form.Control as="select" custom name="gender" onChange={handleChange} value={formUser.gender}>
                    <option value="">--SEXO--</option>
                    <option value="MALE">MASCULINO</option>
                    <option value="FEMALE">FEMININO</option>
                    <option value="NAN">UNDEFINED</option>
                </Form.Control>
            </Form.Group>

                <hr />

                <Form.Group >
                    {isUpdate && !updatePhoto ? (
                        <Pictur>
                            <img src={formUser.photo} alt={formUser.name} />
                            <span onClick={removePhoto}>Remover</span>
                        </Pictur>
                    ) : (
                            // <>
                            //     <input name="photo" type="file" onChange={handleChange} />
                            // </>


<>

<PhotoButton onClick={clickopenfile}  variant="primary">{picture}</PhotoButton>

<Inputf>
    <input ref={inputEl} id="inpute"  name="photo" type="file" onChange={handleChange} />
    </Inputf>

    <Pictur>
        {previewPhoto ? <span onClick={removePhoto}>Remover</span> : ""}
        {previewPhoto ? <img src={previewPhoto} alt={formUser.name} /> : ""}
    </Pictur>
</>


                        )}
                </Form.Group>

               

                <Form.Group >
                    <Button variant="primary" disabled={isNotValid()} onClick={submitUser}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : ""}

                        {isUpdate ? "Atualizar" : "Cadastrar"}

                    </Button>
                </Form.Group>
                <br />
                {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
                <br />
         </Col> 
         </Row>
         </Form>
                   
                        
        </div>
    )

}

export default UsersForm



const Pictur = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red 
        }
    }


`
const PhotoButton = styled(Button)`
width: 95px;
`
const Inputf = styled.div`
cursor: pointer;
input[type="file"] {
    display: none; 
}
`