import React, { useState, useEffect } from 'react'
import { Button, Spinner, Table, Col, Row, Form } from 'react-bootstrap'
import { FaRegEdit, FaTrashAlt } from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { getUser } from '../../config/auth'
import { disciplinaDrop, disciplinasListAll } from '../../store/disciplinas/disciplinas.actions'
import { swalConfirmation } from '../../util'


/**Userlist do painel do admin */
const ListDisciplina = (props) => {

    const [reloadlist, setreloadlist] = useState(0)
    const [formFilter, setFilter] = useState({})

    const disciplinas_redux = useSelector(state => state.disciplinas.disciplinasAll)
    const [filtredList, setfiltredList] = useState(disciplinas_redux)
    const org = getUser()

    /**Garante que a oganização será sempre apenas a do user logado */
    const [queryDisciplina, setqueryDisciplina] = useState({
        organizacao: org.organizacao
    })

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(disciplinasListAll(queryDisciplina))
    }, [dispatch, reloadlist])


    //Useefect necessário para chamar a lista sem filtro assim que carrega
    //o componente
    useEffect(() => {
        setfiltredList(disciplinas_redux)
        refreshFilter()
    }, [disciplinas_redux])

    const handleChange = (attr) => {
        const { value, name } = attr.target

        setFilter({
            ...formFilter,
            organizacao: org.organizacao,
            [name]: value
        })

        return;
    }
    const multiFilter = (arr, iformFilter = formFilter) => {

        let tempArr = arr

        for (let [keyFilter, value] of Object.entries(iformFilter)) {

            if (keyFilter !== "organizacao") {
                tempArr = tempArr.filter(item => item[keyFilter] === value)
            }
        }


        setfiltredList(tempArr)
    }

    const apagarItem = (user) => {
        swalConfirmation(`Excluir a Disciplina ${user.name}?`,
            () => {
                dispatch(disciplinaDrop(user._id))
                setreloadlist(reloadlist + 1)
            })
    }

    const refreshFilter = () => {
        const x = multiFilter(disciplinas_redux)
    }


    /**Faz o setfilter vazio */
    const clearFilter = () => {
        setfiltredList(disciplinas_redux)
        setFilter({})
    }


    return (
        <div className="alturaminima">
            <br />
            <h4>Lista de Disciplinas:</h4>
            <br />

            
                <Form>
                <Row xs="1" sm="1" md="4" lg="4" xl="4">

                        <Col>
                            <Form.Group >
                                <Form.Control type="number" onChange={handleChange} name="yearstage" value={formFilter.yearstage || ""}
                                    placeholder="yearstage" />
                            </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group>
                            <Form.Control type="number" onChange={handleChange} name="yearcalendar" value={formFilter.yearcalendar || ""}
                                placeholder="yearcalendar Ano letivo (2020, 2021.." />
                                </Form.Group>
                        </Col>

                        <Col>
                        <Form.Group>
                            <Button block variant="primary" onClick={refreshFilter}>  Filtrar </Button>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group >
                                <Button block variant="primary" onClick={clearFilter}>  Remover filtro </Button>
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>
            
            <Scrolable>
                <Table striped hover>
                    <thead>
                        <tr>
                            <THeadItem>Série</THeadItem>
                            <THeadItem>Ano Letivo</THeadItem>
                            <THeadItem>Nome</THeadItem>
                            <THeadItem>Id</THeadItem>
                            <THeadItem>Ações</THeadItem>

                        </tr>
                    </thead>
                    <tbody>

                        {disciplinas_redux.length <= 0 ? <Spinner animation="grow" role="status" /> : ""}

                        {filtredList.map((turma, i) => (
                            <tr key={i}>
                                <td>{turma.yearstage}</td>
                                <td>{turma.yearcalendar}</td>
                                <td>{turma.name}</td>
                                <td>{turma._id}</td>
                                <TDItem>

                                    <ActionButton onClick={() => props.updateUser(turma)} variant="info" size="sm"><FaRegEdit /></ActionButton>
                                    |

                                    <ActionButton onClick={() => apagarItem(turma)} variant="danger" size="sm"><FaTrashAlt /></ActionButton>
                                </TDItem>
                            </tr>
                        ))}

                    </tbody>
                </Table>
            </Scrolable>
        </div>
    )
}



export default ListDisciplina

const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: auto;    
    padding: 1px;

    table{
        tbody{
            tr{
                td{
                    text-align:center;
                }
            }
        }
    }
`
const THeadItem = styled.th`
    background: #666;
    color:#eee;
text-align:center;

    :nth-child(1){  width: 10%; }
    :nth-child(2){  width: 10%; }
    :nth-child(3){  width: 40%; }
    :nth-child(4){  width: 20%; }
    :nth-child(5){  width: 20%; }
`

const TDItem = styled.td`
    display: flex;
    justify-content :center;
    
`

const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`

