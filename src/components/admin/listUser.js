import React, { useState, useEffect } from 'react'
import { Button, Spinner, Table, Col, Row, Form } from 'react-bootstrap'
import { FaRegEdit, FaTrashAlt, FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { getUser } from '../../config/auth'
import { userDrop, userListAll } from '../../store/users/users.actions'
import { swalConfirmation } from '../../util'


/**Userlist do painel do admin */
const ListUsers = (props) => {

    const [reloadlist, setreloadlist] = useState(0)
    const [formFilter, setFilter] = useState({})
    const users_redux = useSelector(state => state.user.usersAll)
    const [filtredList, setfiltredList] = useState(users_redux)
    const org = getUser()


    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(userListAll())
    }, [dispatch, reloadlist])


    useEffect(() => {
        setfiltredList(users_redux)
        refreshFilter()
    }, [users_redux])

    const apagarUsuario = (user) => {
        swalConfirmation(`Excluir o usuário ${user.nome}?`,
            () => {
                dispatch(userDrop(user._id))
                setreloadlist(reloadlist + 1)
            })
    }

    const handleChange = (attr) => {
        const { value, name } = attr.target

        setFilter({
            ...formFilter,
            organizacao: org.organizacao,
            [name]: value
        })

        return;
    }


    /**Aplica o filtro em users_redux e atribui o valor filtrado em filtred list  */
    const multiFilter = (arr, iformFilter = formFilter) => {
        let tempArr = arr
//{"organizacao": ObjectId("5fdf4e69ecf8b11a2894a64d")}
        for (let [keyFilter, value] of Object.entries(iformFilter)) {

            if (keyFilter === "number") { //Precisa de tratamento diferenciado p objeto aninhado
                tempArr = tempArr.filter(item => item.turma.number === value)
            } else if (keyFilter === "yearcalendar") {
                tempArr = tempArr.filter(item => item.turma.yearcalendar === value)
            }else{
                tempArr = tempArr.filter(item => item[keyFilter] === value)
            }
        }
        setfiltredList(tempArr)
    }

    const refreshFilter = () => {
        const x = multiFilter(users_redux)
    }


    /**Faz o setfilter vazio */
    const clearFilter = () => {
        setfiltredList(users_redux)
        setFilter({})
    }


    return (
        <div className="alturaminima">
        <br />
        <h4>Lista de Usuários:</h4>
        <br />
           
           
                <Form>
                    <Row xs="1" sm="1" md="5" lg="5" xl="5">

                        <Col>
                            <Form.Group >
                                <Form.Control autocomplete="off" type="text"  autocomplete="off" onChange={handleChange} name="number" value={formFilter.number || ""}
                                    placeholder="Turma, (802, 501...)"  />

                                    
                            </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group >
                            <Form.Control autocomplete="off" type="text" onChange={handleChange} name="tipo" value={formFilter.tipo || ""}
                                placeholder="Tipo (ALUNO, PROFESSOR, ADM... )." />


                                </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group>
                            <Form.Control autocomplete="off" type="number" onChange={handleChange} name="yearcalendar" value={formFilter.yearcalendar || ""}
                                placeholder="Ano letivo (2020, 2021.." />
                                </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group>
                            <Button block variant="primary" onClick={refreshFilter}>  Filtrar </Button>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group >
                                <Button block variant="primary" onClick={clearFilter}>  Reset </Button>
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>
           

            <Scrolable>
                <Table striped hover>
                    <thead>
                        <tr>
                            <THeadItem>Nome</THeadItem>
                            <THeadItem>Função</THeadItem>
                            <THeadItem>Email</THeadItem>
                            <THeadItem>Id</THeadItem>
                            <THeadItem>Turma</THeadItem>
                            <THeadItem>Ano Letivo</THeadItem>
                            <THeadItem>Ativo</THeadItem>
                            <THeadItem>Ações</THeadItem>

                        </tr>
                    </thead>
                    <tbody>

                        {users_redux.length <= 0 ? <Spinner animation="grow" role="status" /> : ""}
                        {users_redux.length <= 0 ? <> <br/> Certifique-se que há usuários cadastrados....</> : ""}
                        {filtredList.map((catg, i) => (
                            <tr key={i}>
                                <td>{catg.nome}</td>
                                <td>{catg.tipo}</td>
                                <td>{catg.email}</td>
                                <td>{catg._id}</td>
                                <td>{catg.turma?.number}</td>
                                <td>{catg.turma?.yearcalendar}</td>
                                <td>{catg.is_active ? <FaCheckCirclegreen /> : <FaTimesCirclered />}</td>
                                <TDItem>

                                    <ActionButton onClick={() => props.updateUser(catg)} variant="info" size="sm"><FaRegEdit /></ActionButton>
                                    |

                                    <ActionButton onClick={() => apagarUsuario(catg)} variant="danger" size="sm"><FaTrashAlt /></ActionButton>
                                </TDItem>
                            </tr>
                        ))}

                    </tbody>
                </Table>
            </Scrolable>
        </div>
    )
}



export default ListUsers

const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: auto;    
    padding: 1px;
    table{
        tbody{
            tr{
                td{
                    text-align:center;
                }
            }
        }
    }
`
const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;
    :nth-child(1){  width: 10%; }
    :nth-child(2){  width: 10%; }
    :nth-child(3){  width: 5%; }
    :nth-child(6){  width: 50%; }
    :nth-child(7){  width: 20%; }
`

const TDItem = styled.td`
    display: flex;
    justify-content :center;
`
const FaCheckCirclegreen = styled(FaCheckCircle)`
color:green;
`

const FaTimesCirclered = styled(FaTimesCircle)`
color:red;
`
const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`

