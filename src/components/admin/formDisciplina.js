import React, { useState, useEffect } from 'react'
import { Form, Button, Spinner, Col, Row, } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import { userListAll } from '../../store/users/users.actions';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { createDisciplina, updateDisciplina } from '../../services/admin'
import { getOrg, getUser } from '../../config/auth';


const FormDisciplina = (props) => {
    const org = getOrg()

    const [reloadlist, setreloadlist] = useState(0)
    const [filterTurma, setfilterTurma] = useState("802")

    const users_redux = useSelector(state => state.user.usersAll)
    const [isProcessando, setisProcessando] = useState(false)


    const [formDisciplin, setFormDisciplin] = useState({
        ...props.update,
        turma: props?.update?._id || "",
        organizacao: org,
        cadastradoPor: getUser().id
    })



    const clearForm = () => {
        setFormDisciplin({})
    }

    function filtrarDisciplina(item) {
        return item.tipo === "ALUNO" && item.turma.number === filterTurma
    }


    const typeReq = (data) => isUpdate ? updateDisciplina(props.update._id, data) : createDisciplina(data)


    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(userListAll())
    }, [dispatch, reloadlist])


    const handleChange = (attr) => {
        let { value, name } = attr.target

        name === "name" ? value = value.toUpperCase() : value = value

        setFormDisciplin({
            ...formDisciplin,
            organizacao: org,
            [name]: value
        })

        return;
    }
    const submitUser = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })

        let data = new FormData()

        Object.keys(formDisciplin)
            .forEach(key => data.append(key, formDisciplin[key]))


        await typeReq(data)
            .then((res) => {
                clearForm()
                message('success', `Disciplina Cadastrada com sucesso.`)
                setisProcessando(false)
            })
            .catch((err) => {
                message('error', `Erro ao cadastrar Disciplina.${err.message}`)
                clearForm()
            })

        setisProcessando(false)

    }

    const isUpdate = Object.keys(props.update).length > 0

    const isNotValid = () => {
        return !formDisciplin.yearcalendar || !formDisciplin.name || !formDisciplin.yearstage || !formDisciplin.topico
    }


    return (
        <div className="alturaminima">

            <br />
            <h4>Cadastro de Disciplina:</h4>
            <br />
            <Form>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={8} xl={6}>
                        <Form.Group >
                            <Form.Control onkeydown="upperCaseF(this)" type="text" onChange={handleChange} name="name" value={formDisciplin.name || ""}
                                placeholder="Nome" />
                        </Form.Group>

                        <Form.Group >
                            <Form.Control type="text" onChange={handleChange} name="topico" value={formDisciplin.topico || ""}
                                placeholder="Tópico, ex.: Regra de Três.." />
                        </Form.Group>

                        <Form.Group >
                            <Form.Control type="number" onChange={handleChange} name="yearcalendar" value={formDisciplin.yearcalendar || ""}
                                placeholder="yearcalendar Ano letivo (2020, 2021.." />
                        </Form.Group>

                        <Form.Group >
                            <Form.Control type="number" onChange={handleChange} name="yearstage" value={formDisciplin.yearstage || ""}
                                placeholder="yearstage" />
                        </Form.Group>

                        <Form.Group >
                            <Button variant="primary" disabled={isNotValid()} onClick={submitUser}>
                                {isProcessando ? <Spinner animation="grow" role="status" /> : ""}

                                {isUpdate ? "Atualizar" : "Cadastrar"}

                            </Button>
                        </Form.Group>
                    </Col>
                </Row>
            </Form>




        </div>
    )
}

export default FormDisciplina


