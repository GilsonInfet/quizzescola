import React, { useState, useEffect } from 'react'
import { Form,  Col, Row, Button, Spinner } from 'react-bootstrap'
import { useDispatch } from 'react-redux';
import { FaCheckCircle, FaTimesCircle } from 'react-icons/fa'

import styled from 'styled-components';
import { userListAll } from '../../store/users/users.actions';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { createTurma, updateTurma  } from '../../services/admin'
import { getUser, getOrg } from '../../config/auth';


const FormTurma = (props) => {


    const [reloadlist, setreloadlist] = useState(0)

    const [isProcessando, setisProcessando] = useState(false)
 
    const [formTurma, setFormTurma] = useState({
        ...props.update,
        turma : props?.update?.turma?._id || "",
           organizacao : getOrg() 
    })

    const isNotValid = () => {
        return !formTurma.yearcalendar || !formTurma.number
    }

    const clearForm = () => {
        setFormTurma({})
    }
    const isUpdate = Object.keys(props.update).length > 0

    const typeReq = (data) => isUpdate ? updateTurma(props.update._id, data) : createTurma(data)

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(userListAll())
    }, [dispatch, reloadlist])

    const org = getUser()

    const handleChange = (attr) => {
        const { value, name } = attr.target

        setFormTurma({
            ...formTurma,
            organizacao: org.organizacao,
            [name]: value
        })

        return;
    }
    const submitUser = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })

        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(formTurma)
            .forEach(key => data.append(key, formTurma[key]))


        await typeReq(data,)
            .then((res) => {
                clearForm()
                message('success', `Turma Cadastrada com sucesso.`)
                setisProcessando(false)
            })
            .catch((err) => {
                message('error', `Erro ao cadastrar turma.${err.message}`)
                clearForm()
            })


        setisProcessando(false)


    }

    


    return (
        <div className="alturaminima">
            <br />
            <h4>Cadastro de Turma:</h4>
            <br />

            <Form>
            <Row xs="1" sm="1" md="5" lg="5" xl="5">

                    <Col>
                        <Form.Group >
                            <Form.Control type="number" onChange={handleChange} name="number" value={formTurma.number || ""}
                                placeholder="Nº da turma, (802, 501..." />
                        </Form.Group>
                    </Col>
                    <Col>
                    <Form.Group>
                        <Form.Control type="number" onChange={handleChange} name="yearcalendar" value={formTurma.yearcalendar || ""}
                            placeholder="Ano letivo (2020, 2021.." />
                            </Form.Group>
                    </Col>

                    <Col>
                        <Form.Group >
                            <Button variant="primary" disabled={isNotValid()} onClick={submitUser}>
                                {isProcessando ? <Spinner animation="grow" role="status" /> : ""}

                                {isUpdate ? "Atualizar" : "Cadastrar"}

                            </Button>
                        </Form.Group>
                    </Col>
                </Row>
            </Form>
            


        </div>
    )
}

export default FormTurma

const THeadItem = styled.th`
    background: #666;
    color:#eee;

    :nth-child(1){  width: 20%; }
    :nth-child(2){  width: 20%; }
    :nth-child(3){  width: 20%; }
    :nth-child(4){  width: 20%; }
    :nth-child(5){  width: 20%; }
`

const TDItem = styled.td`
    display: flex;
    justify-content :center;
`
const FaCheckCirclegreen = styled(FaCheckCircle)`
color:green;
`

const FaTimesCirclered = styled(FaTimesCircle)`
color:red;
`