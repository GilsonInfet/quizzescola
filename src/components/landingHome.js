
import React, { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap'
import styled from 'styled-components';
import loginBgir from '../assets/images/blackboard.JPG'


/**Page de entrada logo após logar. Receberá avisos gerais. Independe o role */
const LandingHome = () => {

const [y, setY] = useState(12)

function Mensagens () {

        return (
            <>           
               
                <p style={{color:`white`}} >  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis suscipit ante, sit
                            amet tristique mi. Integer malesuada quis tellus id tempus. Ut laoreet at tortor sed viverra.
                            Fusce est purus, ullamcorper mollis varius sed, ultrices a lectus. Nulla facilisi. Integer mollis,
                            sapien sit amet porta ornare, urna massa scelerisque neque, sed faucibus nulla magna at mi.
                            Nam dolor sapien, sagittis a nisi ac, dignissim iaculis urna. Sed ullamcorper turpis id maximus
                            euismod. Fusce a quam id quam ultricies fringilla. Nulla tortor mi, vulputate dictum rutrum sit amet,
                            placerat eleifend ante. Donec consectetur posuere lorem, blandit varius diam imperdiet quis.
             In blandit vehicula tempus. Maecenas magna nibh, fermentum in commodo et, scelerisque vitae est. </p>
           
            </>
        )
    }

    function scrollFunction() {
     
        setY(window.pageYOffset)
     }


    useEffect(() => {
        window.addEventListener('scroll', scrollFunction);
 return ()=> window.removeEventListener('scroll', scrollFunction);
    }, [])

    return (
        <>         
         <Imgdiv 
         style = {{transform:`translateY(${y*-0.05}px)` }} >
             <Container>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           <Mensagens/>
           
           </Container>
            </Imgdiv>
                 
        </>
    )
}

export default LandingHome
//Paralax effect co jquery
//https://www.youtube.com/watch?v=K8v5ycE262s&feature=emb_logo


const Imgdiv = styled.div`

        background-image: url(${loginBgir})  ; 
      
        background-repeat: repeat-y;        
        background-attachment: fixed;
        background-color: black;

        background-size: cover ;
        
        `

