import React, { useState, useEffect, useRef } from 'react'
import { Button, Col, Form, ProgressBar, Row, Spinner, Table } from 'react-bootstrap'
import Swal, { swal } from 'sweetalert2/dist/sweetalert2.js'
import { createPergunta, updatePergunta } from '../../services/admin'
import styled from 'styled-components'
import { getOrg, getUser } from '../../config/auth'
import { useDispatch, useSelector } from 'react-redux'
import { FaPlus, FaMinusCircle,  FaFileImport } from 'react-icons/fa'
import { disciplinasListAll } from '../../store/disciplinas/disciplinas.actions'
import { message, bases3 } from '../../util/index'


const QuestoesForm = (props) => {
    const inputEl = useRef(null)
    const picture = <FaFileImport/>

    const org = getOrg()
    const currentUser = getUser()
    const [formItem, setFormItem] = useState({
        ...props.update,
        disciplina: props.update?.disciplina?._id || "",
        resposta_certa: props.update.resposta_certa || [],
        resposta_errada: props.update.resposta_errada || [],
        organizacao: getOrg(),
        cadastradoPor: currentUser.id
    })

    const [progress, setProgress] = useState(0)
    const [updatePhoto, setUpdatePhoto] = useState(false)
    const [isProcessando, setisProcessando] = useState(false)
    const [tempOption, setTempOption] = useState({
        correta: "",
        errada: ""
    })

    const [previewPhoto, setPrewiewPhoto] = useState(null)
    const disciplinas_redux = useSelector(state => state.disciplinas.disciplinasAll)

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(disciplinasListAll({ organizacao: org.organizacao }))
    }, [dispatch])

    useEffect(() => {

    }, [progress])

    const isUpdate = Object.keys(props.update).length > 0

    /**isUpdate ? updateUser(props.update._id, data) : createUser(data) */
    const typeReq = (data, config) => isUpdate ? updatePergunta(props.update._id, data, config) : createPergunta(data, config)

    const handleChange = (attr) => {
        const { value, name } = attr.target
        // debugger
        // /yearstage} Ano Calendar: - {disciplina.yearcalendar


        if (name === 'photo') {

            try {
                setPrewiewPhoto(URL.createObjectURL(attr.target.files[0]) || null)
            } catch (error) {
                setPrewiewPhoto(null)
            }

            setFormItem({
                ...formItem,
                'photo': attr.target.files[0]
            })
        } else if (name === 'disciplina') {
                setFormItem({
                    ...formItem,
                    disciplina :value,
                    yearcalendar: disciplinas_redux.find(v => v._id === value).yearcalendar,
                    yearstage: disciplinas_redux.find(v => v._id === value).yearstage
                })
            }else{

                setFormItem({
                    ...formItem,
                    [name]: value
                })

        }
        return;
    }

    const submitCreate = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })

        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(formItem)
            .forEach(key => {

                data.append(key, formItem[key])

            })

        if (typeof formItem.photo === "string") {
            data.delete('photo')
        }


        data.delete('resposta_certa')
        data.delete('resposta_errada')

        data.append('resposta_certa', JSON.stringify(formItem.resposta_certa))
        data.append('resposta_errada', JSON.stringify(formItem.resposta_errada))

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        typeReq(data, config)
            .then((res) => {
                clearForm()
                message('success', `Pergunta Cadastrada com sucesso.`)
                setisProcessando(false)

            })
            .catch((err) => {
                message('error', `Erro ao cadastrar Pergunta.${err.message}`)
                clearForm()
            })
        setisProcessando(false)


    }

    const clearForm = () => {
        setProgress(0)
        setUpdatePhoto(true)

        //Limpa sem remover as configurações mínimas
        setFormItem({
            resposta_certa: [],
            resposta_errada: [],
            organizacao: getOrg(),
            cadastradoPor: currentUser.id
        })
        setPrewiewPhoto(null)
    }


    /**Vai ser usado no disabled button, logo se alguma das condições retornar true desabilita o botão
     * Video e photo não obrigatório
    */
    const isNotValid = () => {
        return  (!formItem.yearcalendar)
        || (!formItem.disciplina)                           // Object.keys(formItem).some(k => typeof formItem[k] === "string" && formItem[k] === "")
        || (!formItem.organizacao)  
        || (!formItem.cadastradoPor) 
        || (!formItem.yearstage) 
        || (!formItem.pergunta) 
        || (formItem.resposta_certa.length < 1)
        || (formItem.resposta_errada.length < 4)

    }

    const clickopenfile = ()=>{       inputEl.current.click()   }

    const removePhoto = () => {
        setUpdatePhoto(true)
        setFormItem({
            ...formItem,
            photo: ""
        })
        setPrewiewPhoto(null)
    }

    /**Tipo C ou E e posição */
    const apagarItem = (tipo, pos) => {

        let tempCorr = formItem.resposta_certa.slice()
        let tempErr = formItem.resposta_errada.slice()

        if (tipo === "c") {
            tempCorr.splice(pos, 1)
        } else {
            tempErr.splice(pos, 1)
        }

        setFormItem({
            ...formItem,
            resposta_certa: tempCorr, //opcoesCorretas,
            resposta_errada: tempErr,
        })

        setTempOption({})
    }


    /**Botão Add tipo c ou não c */
    const pushOnState = (tipo) => {
        // debugger
        let tempCorr = formItem.resposta_certa.slice()
        let tempErr = formItem.resposta_errada.slice()
        let junctionOptions = tempCorr.concat(tempErr)

        if (validarInsertOpcao(junctionOptions, tempOption.correta) || validarInsertOpcao(junctionOptions, tempOption.errada)) {

            const x = message('error', "Este valor já está inserido numa das opções")

            setTempOption({})
            return
        }

        if (tipo === "c") {
            tempCorr.push(tempOption.correta)
        } else {
            tempErr.push(tempOption.errada)
        }

        setFormItem({
            ...formItem,
            resposta_certa: tempCorr, //opcoesCorretas,
            resposta_errada: tempErr,
        })

        setTempOption({})
    }

    const handleChangetemp = (attr) => {
        const { value, name } = attr.target

        // tempOptio
        if (name === 'correta') {
            setTempOption({
                ...tempOption,
                correta: value
            })
        } else {

            setTempOption({
                ...tempOption,
                errada: value
            })
        }
        return;
    }



    /**optname = c ou e */
    const makeTabela = (dadosTabela, optname) => {

        return (
            <Table striped hover>
                <thead>
                    <tr>
                        <THeadItem>Index</THeadItem>
                        <THeadItem>{optname === "e" ? "Opções Erradas (Mínimo 4)" : "Opções Corretas  (Mínimo 1)"}</THeadItem>
                        <THeadItem>Ações</THeadItem>

                    </tr>
                </thead>
                <tbody>

                    {dadosTabela.map((item, i) => (
                        <tr key={i}>
                            <td>{i}</td>
                            <td>{item}</td>

                            <TDItem>
                                <ActionButton onClick={() => apagarItem(optname, i)} variant="danger" size="sm"><FaMinusCircle /></ActionButton>
                            </TDItem>
                        </tr>
                    ))}

                </tbody>
            </Table>
        )
    }


    const validarInsertOpcao = (arr, item) => {
        return arr.some(elem => elem === item)
    }


    return (
        <>
          

                <br />
                <h4>Cadastro de Questões:</h4>
                <br />

                <Form.Group >
                    <Form.Control as="select" custom name="disciplina" onChange={handleChange} value={formItem.disciplina || ""}>
                        <option value="">--DISCIPLINA--</option>

                        {disciplinas_redux.map((disciplina, i) => (

                            <option key={i} value={disciplina._id}> {disciplina.name} - Ano: {disciplina.yearstage} Ano Calendar: - {disciplina.yearcalendar}  </option>

                        ))}


                    </Form.Control>
                </Form.Group>


                <Row>
                    <Col>
                        <Form.Group >

                            <Form.Control type="text" onChange={handleChange} name="yearstage" value={formItem.yearstage || ""}
                              readOnly  placeholder="yearstage" />

                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group >

                            <Form.Control type="text" onChange={handleChange} name="yearcalendar" value={formItem.yearcalendar || ""}
                                 readOnly placeholder="yearcalendar" />

                        </Form.Group>
                    </Col>
                </Row>

                <Form.Group >
                    <Form.Control autocomplete="off" type="text" as="textarea" rows={3} onChange={handleChange} name="pergunta" value={formItem.pergunta || ""}
                        placeholder="pergunta / Enunciado" />
                </Form.Group>


                <Form.Group >

                    <Form.Control type="text" onChange={handleChange} name="video" value={formItem.video || ""}
                        placeholder="Link para o vídeo " />

                </Form.Group>

                <span >Imagem auxiliar para a questão</span>
                <Form.Group >

                    {isUpdate && !updatePhoto ? (
                        <>
                                                    <Inputf>
                                                    <input ref={inputEl} id="inpute"  name="photo" type="file" onChange={handleChange} />
                                                    </Inputf>
                        <Pictur>
                            <img src={bases3 + formItem.photo} alt={formItem.name} />
                            <span onClick={removePhoto}>Remover</span>
                        </Pictur>
                        </>
                    ) : (
                            <>

                            <PhotoButton onClick={clickopenfile}  variant="primary">{picture}</PhotoButton>

                            <Inputf>
                                <input ref={inputEl} id="inpute"  name="photo" type="file" onChange={handleChange} />
                                </Inputf>

                                <Pictur>
                                    {previewPhoto ? <span onClick={removePhoto}>Remover</span> : ""}
                                    {previewPhoto ? <img src={previewPhoto} alt={formItem.name} /> : ""}
                                </Pictur>
                            </>
                        )}
                </Form.Group>

                {makeTabela(formItem.resposta_certa, "c")}

                <Row>
                    <Col xs={10}>
                        <Form.Group >
                            <Form.Control autocomplete="off" type="text" onChange={handleChangetemp} name="correta" value={tempOption.correta || ""}
                                placeholder="Opção Certa" />
                        </Form.Group>
                    </Col>
                    <Col >
                        <Button block variant="primary" disabled={!tempOption.correta} onClick={() => pushOnState("c")}><FaPlus /> </Button>
                    </Col>
                </Row>

                <hr />

                {makeTabela(formItem.resposta_errada, "e")}

                <Row>
                    <Col xs={10}>
                        <Form.Group >
                            <Form.Control autocomplete="off" type="text" onChange={handleChangetemp} name="errada" value={tempOption.errada || ""}
                                placeholder="Opção Errada" />
                        </Form.Group>
                    </Col>
                    <Col >
                        <Button block variant="primary" disabled={!tempOption.errada} onClick={() => pushOnState("e")}><FaPlus /> </Button>
                    </Col>
                </Row>



                <hr />





                <Form.Group >

                    <Button variant="primary" disabled={isNotValid()} onClick={submitCreate}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : ""}

                        {isUpdate ? "Atualizar" : "Cadastrar"}

                    </Button>
                </Form.Group>
                <br />
                {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
                <br />
            
        </>
    )

}

export default QuestoesForm

const DivPad = styled.div`
padding-left: 20px;
padding-top: 10px;
margin-bottom:15px;
background-color:white;
line-height: 1.5;
border-radius: 5px;
border: 1px solid #ccc;
box-shadow: 1px 1px 1px #999;
`
const TDItem = styled.td`
    display: flex;
    justify-content :center;    
`
const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`

const Inputf = styled.div`
cursor: pointer;
input[type="file"] {
    display: none; 
}
`



const PhotoButton = styled(Button)`
width: 95px;
`

const Pictur = styled.div`
    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red 
        }
    }
`
const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;

    :nth-child(1){  width: 5%; }
    :nth-child(2){  width: 90%; }
    :nth-child(3){  width: 5%; }

`
