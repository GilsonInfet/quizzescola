import React, { useState, useEffect } from 'react'
import { Button, Spinner, Table, Col, Row, Form } from 'react-bootstrap'
import { FaRegEdit, FaTrashAlt} from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { getUser } from '../../config/auth'
import { perguntasListAll, perguntaDrop} from '../../store/perguntas/perguntas.actions'
import { swalConfirmation } from '../../util'
import Swal from 'sweetalert2'
import { bases3} from '../../util/index'

/**Userlist do painel do admin */
const ListQuestoes = (props) => {

    
    const [reloadlist, setreloadlist] = useState(0)
    const [formFilter, setFilter] = useState({})

    const perguntas_redux = useSelector(state => state.perguntas.perguntasAll)
    const [filtredList, setfiltredList] = useState(perguntas_redux)
    const org = getUser()

     const dispatch = useDispatch()
    useEffect(() => {
        dispatch(perguntasListAll())
    }, [dispatch, reloadlist])


    //Useefect necessário para chamar a lista sem filtro assim que carrega
    //o componente
    useEffect(() => {
        setfiltredList(perguntas_redux)
        refreshFilter()
    }, [perguntas_redux])

    const handleChange = (attr) => {
        const { value, name } = attr.target

        setFilter({
            ...formFilter,
            organizacao: org.organizacao,
            [name]: value
        })

        return;
    }
    const multiFilter = (arr, iformFilter = formFilter) => {

        let tempArr = arr

        for (let [keyFilter, value] of Object.entries(iformFilter)) {
            if (keyFilter !== "organizacao") {
                tempArr = tempArr.filter(item => item[keyFilter] === value)
            }
        }


        setfiltredList(tempArr)
    }

    const apagarItem = (item) => {
        swalConfirmation(`Excluir o item selecionado ?`,
            () => {
                dispatch(perguntaDrop(item._id))
                setreloadlist(reloadlist + 1)
            })
    }

    const refreshFilter = () => {
        const x = multiFilter(perguntas_redux)
    }

    const _detalhe = (questao) => {

        Swal.fire({

            title: questao._id,

            imageUrl: bases3+questao.photo,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            html:
                `Enunciado : ${JSON.stringify(questao)} <br/>`
        })
    }

    /**Faz o setfilter vazio */
    const clearFilter = () => {
        setfiltredList(perguntas_redux)
        setFilter({})
    }


    const arrStrToList = (exer) => {
 
        const mont = () => exer.map((item, index) => (
            <li key={index}>{item}</li>
        ))

        return (
            <UlStyled >
                {mont()}
            </UlStyled>
        )

    }


    return (
        <>
              <br />
                <h4>Lista de Questões:</h4>
                <br />

            
                <Form>
                <Row xs="1" sm="1" md="4" lg="4" xl="4">

                        <Col>
                            <Form.Group >
                                <Form.Control type="number" onChange={handleChange} name="yearstage" value={formFilter.yearstage || ""}
                                    placeholder="yearstage" />
                            </Form.Group>
                        </Col>
                        <Col>
                        <Form.Group>
                            <Form.Control type="number" onChange={handleChange} name="yearcalendar" value={formFilter.yearcalendar || ""}
                                placeholder="yearcalendar Ano letivo (2020, 2021.." />
                                </Form.Group>
                        </Col>

                        <Col>
                        <Form.Group>
                            <Button block variant="primary" onClick={refreshFilter}>  Filtrar </Button>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group >
                                <Button block variant="primary" onClick={clearFilter}>  Remover filtro </Button>
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>
            
            <Scrolable>
                <Table striped hover  >
                    <thead>
                        <tr>
                            {/* <THeadItem>Id</THeadItem>  */}
                            <THeadItem>Série</THeadItem>
                            <THeadItem>Ano Letivo</THeadItem>
                            <THeadItem>Disciplina</THeadItem>
                            <THeadItem>Enunciado</THeadItem>
                            <THeadItem>Imagem</THeadItem>
                            <THeadItem>Video</THeadItem>
                            <THeadItem>Opções Corretas</THeadItem>
                            <THeadItem>Opções Erradas</THeadItem>
                            <THeadItem>Ações</THeadItem>

                        </tr>
                    </thead>
                    <tbody>

                        {perguntas_redux.length <= 0 ? <Spinner animation="grow" role="status" /> : ""}

                        {filtredList.map((pergunta, i) => (
                            <tr key={i}>
                                {/* <td>{pergunta._id}</td> */}
                                <td>{pergunta.yearstage}</td>
                                <td>{pergunta.yearcalendar}</td>
                                <td>{`${pergunta.disciplina.yearcalendar} - ${pergunta.disciplina.yearstage} - ${pergunta.disciplina.name}`}</td>
                                <td>{pergunta.pergunta}</td>
                                <td>{pergunta.photo ? <Responsive onClick={() => _detalhe(pergunta)} src={bases3+pergunta.photo} alt="Imagem da pergunta" /> : "N/A"}</td>
                                <td>{pergunta.video ? <a href= {pergunta.video} target='_blank'>Link Video</a> : "N/A"}</td>
                                <td>{arrStrToList(pergunta.resposta_certa)}</td>
                                <td>{arrStrToList(pergunta.resposta_errada)}</td>
                                
                                <TDItem>

                                    <ActionButton onClick={() => props.updateUser(pergunta)} variant="info" size="sm"><FaRegEdit /></ActionButton>
                                    |

                                    <ActionButton onClick={() => apagarItem(pergunta)} variant="danger" size="sm"><FaTrashAlt /></ActionButton>
                                </TDItem>
                            </tr>
                        ))}

                    </tbody>
                </Table>
            </Scrolable>
        </>
    )
}



export default ListQuestoes

const UlStyled = styled.ul`
	/* list-style: none;  */
	margin-left: 0;
	padding-left: 30px;
    text-align:left;
`

const Responsive = styled.img`
width: auto;
height: 60px;
margin:3px;

`


const Scrolable = styled.div` 
resize:both;                   
    min-width: 50%;
    max-width: 100%;
    overflow:auto;
    height: auto;
    /* overflow-x: auto;
    overflow-y: auto;     */
    padding: 0px;

border: 1px solid rgba(102, 102, 102, 0.16);
    table{
        tbody{
            tr{
                td{
                    text-align:center;
                    border: 1px solid white;
                    /* vertical-align:sub; */
                }
            }
        }
    }
`
const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;

    :nth-child(1){  width: 5%; } //serie
    :nth-child(2){  width: 5%; } //ano letivo
    :nth-child(3){  width: 15%; } //enunciado
    :nth-child(4){  width: 30%; } //enunciado
    :nth-child(5){  width: 5%; } //imaghem
    :nth-child(6){  width: 5%; } //video
    :nth-child(7){  width: 20%; } //opt corretas
    :nth-child(8){  width: 20%; } //opt err
    :nth-child(9){  width: 5%; } //ações
`

const TDItem = styled.td`
    display: flex;
    justify-content :center;
    border: 1px solid transparent !important ;
    
`

const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`

