import { message } from "../../util";
import { createTurma,   updateTurma,  deleteTurma,  getWhatIWant } from "../../services/admin";
import { getUser } from "../../config/auth";

export const TURMAS_LIST = "TURMAS_LIST"
export const TURMAS_LIST_ALL = "TURMAS_LIST_ALL"

export const TURMAS_CREATE = "TURMAS_CREATE"
export const TURMAS_UPDATE = "TURMAS_UPDATE"
export const TURMAS_LOADING = "TURMAS_LOADING"

export const turmaSave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createTurma(props)
            dispatch({ type: TURMAS_CREATE, data });
         
        } else {
             const { data } = await updateTurma(id, props)
             .then((res)=>{
             dispatch(turmasListAll())
             dispatch({ type: TURMAS_LOADING })
             })
        }
    };
};


/**Pede a atualização da lista de tumas em store, 
 * já retorna a lista das turmas apenas da org do user */
export const turmasListAll = props => {
    return async (dispatch) => {
        dispatch({ type: TURMAS_LOADING })
        // const { data } = await getTurma(props)
        const { data } = await getWhatIWant('turma', "organizacao=" + getUser().organizacao)
        dispatch({ type: TURMAS_LIST_ALL, data })
        dispatch({ type: TURMAS_LOADING })
    };
}



/**Deleta uma turma com ID mongo dado */
export const turmaDrop = props => {
    return async (dispatch) => {

        dispatch({ type: TURMAS_LOADING })

        deleteTurma(props)
            .then(res => {
                if (res.status === 200) {
                    dispatch(turmasListAll())
                    dispatch({ type: TURMAS_LOADING })
                    message('success', `Turma excluída com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
