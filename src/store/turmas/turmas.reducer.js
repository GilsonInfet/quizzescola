import {
    TURMAS_LOADING,
    TURMAS_CREATE,
    TURMAS_LIST_ALL
} from './turmas.actions'

const INITIAL_STATE = {
    turmasAll: [],
    loading: false
}

const reducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case TURMAS_LOADING:
            state.loading = !state.loading
            return state
        case TURMAS_LIST_ALL:
            state.turmasAll = action.data
            return state    
        case TURMAS_CREATE:
            state.turmasAll.push(action.data)
            return state
        default:
            return state
    }
}

//importado por src\store\index.js
export default reducer
