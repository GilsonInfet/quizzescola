import {
    DISCIPLINAS_LOADING,
    DISCIPLINAS_CREATE,
    DISCIPLINAS_LIST_ALL
} from './disciplinas.actions'

const INITIAL_STATE = {
    disciplinasAll: [],
    loading: false
}

const reducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case DISCIPLINAS_LOADING:
            state.loading = !state.loading
            return state
        case DISCIPLINAS_LIST_ALL:
            state.disciplinasAll = action.data
            return state    
        case DISCIPLINAS_CREATE:
            state.disciplinasAll.push(action.data)
            return state
        default:
            return state
    }
}

//importado por src\store\index.js
export default reducer
