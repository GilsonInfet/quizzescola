import { message } from "../../util";
import { createDisciplina,    deleteDisciplina,    getDisciplina, getWhatIWant } from "../../services/admin";
import history from "../../config/history";
import { getUser } from "../../config/auth";
/**createDisciplina,
deleteDisciplina,
getDisciplina */
export const DISCIPLINAS_LIST = "DISCIPLINAS_LIST"
export const DISCIPLINAS_LIST_ALL = "DISCIPLINAS_LIST_ALL"

export const DISCIPLINAS_CREATE = "DISCIPLINAS_CREATE"
export const DISCIPLINAS_UPDATE = "DISCIPLINAS_UPDATE"
export const DISCIPLINAS_LOADING = "DISCIPLINAS_LOADING"

export const disciplinaSave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createDisciplina(props)
            dispatch({ type: DISCIPLINAS_CREATE, data });
            
        } else {
            // const { data } = await updateUSER(id, props)
            // dispatch({ type: DISCIPLINAS_UPDATE, { name: 1}, id });
        }
    };
};


/**Pede a atualização da lista de disciplina em store, precisa passar no mínimo a organizacao com query */
export const disciplinasListAll = props => {
    return async (dispatch) => {
        dispatch({ type: DISCIPLINAS_LOADING })
        const { data } = await getWhatIWant('disciplina', "organizacao=" + getUser().organizacao)
        // const { data } = await getDisciplina(props)
        dispatch({ type: DISCIPLINAS_LIST_ALL, data })
        dispatch({ type: DISCIPLINAS_LOADING })
    };
}



/**Deleta uma disciplina com ID mongo dado */
export const disciplinaDrop = props => {
    return async (dispatch) => {

        dispatch({ type: DISCIPLINAS_LOADING })

        deleteDisciplina(props)
            .then(res => {
                if (res.status === 200) {
                    dispatch(disciplinasListAll())
                    dispatch({ type: DISCIPLINAS_LOADING })
                    message('success', `Disciplina excluída com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
