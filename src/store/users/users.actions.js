import { message } from "../../util";
import { createUser, deleteUser,  getWhatIWant } from "../../services/admin";
import { getUser } from "../../config/auth";

export const USER_LIST = "USER_LIST"
export const USER_LIST_ALL = "USER_LIST_ALL"

export const USER_CREATE = "USER_CREATE"
export const USER_UPDATE = "USER_UPDATE"
export const USER_LOADING = "USER_LOADING"

export const userSave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createUser(props)
            dispatch({ type: USER_CREATE, data });


        } else {
            // const { data } = await updateUSER(id, props)
            // dispatch({ type: USER_UPDATE, { name: 1}, id });
        }
    };
};



/**Pede a atualização da lista de usuários  em store */
export const userListAll = props => {
    const org = getUser()
    
    return async (dispatch) => {
        dispatch({ type: USER_LOADING })
        const { data } = await getWhatIWant('user', `organizacao=${org.organizacao}`)
        dispatch({ type: USER_LIST_ALL, data })
        dispatch({ type: USER_LOADING })
    };
}

export const userDrop = props => {
    return async (dispatch) => {

        dispatch({ type: USER_LOADING })

        deleteUser(props)
            .then(res => {
                if (res.status === 200) {
                    dispatch(userListAll())
                    dispatch({ type: USER_LOADING })
                    message('success', `Usuário excluído com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
