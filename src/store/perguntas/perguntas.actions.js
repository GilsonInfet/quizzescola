import { message } from "../../util";
import { createPergunta, updatePergunta, deletePergunta, getPergunta } from "../../services/admin";
import history from "../../config/history";
import { getUser } from "../../config/auth";

export const PERGUNTAS_LIST = "PERGUNTAS_LIST"
export const PERGUNTAS_LIST_ALL = "PERGUNTAS_LIST_ALL"

export const PERGUNTAS_CREATE = "PERGUNTAS_CREATE"
export const PERGUNTAS_UPDATE = "PERGUNTAS_UPDATE"
export const PERGUNTAS_LOADING = "PERGUNTAS_LOADING"

export const perguntaSave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createPergunta(props)
            dispatch({ type: PERGUNTAS_CREATE, data });

        } else {
            // const { data } = await updatePergunta(id, props)
            // dispatch({ type: PERGUNTAS_UPDATE, );
        }
    };
};


/**Pede a atualização da lista de tumas em store, precisa passar no mínimo a organizacao com query */
export const perguntasListAll = props => {
    const org = getUser()

    const props2 = {
        ...props,
        organizacao: org.organizacao,
        cadastradoPor: org.role === "PROFESSOR" ? org.id : undefined
    }


    return async (dispatch) => {
        dispatch({ type: PERGUNTAS_LOADING })
        const { data } = await getPergunta(props2)
        dispatch({ type: PERGUNTAS_LIST_ALL, data })
        dispatch({ type: PERGUNTAS_LOADING })
    };
}



/**Deleta uma turma com ID mongo dado */
export const perguntaDrop = props => {
    return async (dispatch) => {

        dispatch({ type: PERGUNTAS_LOADING })

        deletePergunta(props)
            .then(res => {
                if (res.status === 200) {
                    dispatch(perguntasListAll())
                    dispatch({ type: PERGUNTAS_LOADING })
                    message('success', `Pergunta excluída com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
