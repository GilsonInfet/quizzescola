import {
    PERGUNTAS_LOADING,
    PERGUNTAS_CREATE,
    PERGUNTAS_LIST_ALL
} from './perguntas.actions'

const INITIAL_STATE = {
    perguntasAll: [],
    loading: false
}

const reducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case PERGUNTAS_LOADING:
            state.loading = !state.loading
            return state
        case PERGUNTAS_LIST_ALL:
            state.perguntasAll = action.data
            return state    
        case PERGUNTAS_CREATE:
            state.perguntasAll.push(action.data)
            return state
        default:
            return state
    }
}

//importado por src\store\index.js
export default reducer
