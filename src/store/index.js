import { applyMiddleware, combineReducers, createStore } from 'redux'
import Reactotron from '../plugins/ReactotronConfig'
import { composeWithDevTools } from 'redux-devtools-extension'
import turmasReducer from './turmas/turmas.reducer'
import disciplinaReducer from './disciplinas/disciplinas.reducer'
import perguntasReducer from './perguntas/perguntas.reducer'
import userReducer from './users/users.reducer'
import thunk from 'redux-thunk'
import multi from 'redux-multi'


// modularizações dos reduces que vc importou (agrupando-os em um objeto)
const reducers = combineReducers({
    turmas: turmasReducer,
    disciplinas:disciplinaReducer,
    user: userReducer,
    perguntas:perguntasReducer
})

// middlewares de confifurações do projeto
const middleware = [thunk, multi]

// compose que junta os middlewares e ferramentas de debug
const compose = composeWithDevTools(
    applyMiddleware(...middleware),
    Reactotron.createEnhancer()
)

// criação da store
const store = createStore(reducers, compose)

export default store


