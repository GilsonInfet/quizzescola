import axios from 'axios'
import { message } from '../util';
import { getToken, removeToken } from './auth';
import history from './history'

//`https://backexercicios.herokuapp.com/` 
const http = axios.create({
    baseURL: process.env.NODE_ENV === 'development'
        ? `http://localhost:3001/`
        : process.env.REACT_APP_API
})

http.defaults.headers['Content-type'] = 'application/json'
http.defaults.headers["x-forwarded-proto"] = "https"

if (getToken()) {
    http.defaults.headers["x-auth-token"] = getToken();
}
http.interceptors.response.use(
    response => response,
    error => {

        // const status = error.response.status
        const { response: { status } } = error
        console.log(error.message)

        if (error.message === 'Network Error' && !error.message) {
            alert('você está sem internet...reconecte !!!!!')
        }


        switch (status) {
            case 401:
                console.log('Token inválido...')
                removeToken()
                history.push('/login')
                break;

            case 400:
                // alert()
                message('error', "Bad Request.")
                break;

            case 404:
                // alert()
                message('error', "Senha ou email errado.")
                break;
            case 204:

                message('error', "Requisição retornou 204 - No Content")
                break;
            default:
                console.log(status, `aconteceu um erro ${status}`)
                
                break;
        }

        
        return Promise.reject(error)
    }
)

export default http;
