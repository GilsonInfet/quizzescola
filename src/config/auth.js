const TOKEN_KEY = 'QUIZZESCOLA'

/**Retorna o Token armazenado em localstorage */
const getToken = () => {
    const data = JSON.parse(localStorage.getItem(TOKEN_KEY))
    if (data && data.token) {
        return data.token
    }
    return false
}

/**Retorna o objeto User  armazenado em localstorage */
const getUser = () => {
    const data = JSON.parse(localStorage.getItem(TOKEN_KEY))
    if (data && data.user) {
        return data.user
    }
    return false
}

/**Retorna ID MONGO do objeto User  armazenado em localstorage */
const getOrg = () => {
    const data = JSON.parse(localStorage.getItem(TOKEN_KEY))
    if (data && data.user) {
        return data.user.organizacao
    }
    return false
}

const saveToken = (data) => localStorage.setItem(TOKEN_KEY, JSON.stringify(data))

const removeToken = () => localStorage.removeItem(TOKEN_KEY)


/**Devolve true se tem token e se o role do user logado é o mesmo do parâmetro */
const isAuthenticated = (role) => {
    const papel = getUser()
    return getToken() !== false && papel.role === role
}


/**Devolve o tipo do user em letra lowcase. professor, admin aluno... Se não há 
 * role retorna null
 */
const getRole = () => {
    const papel = getUser()
    return papel ? papel.role.toLowerCase() : null
}

export {
    getOrg,
    isAuthenticated,
    getRole,
    getToken,
    getUser,
    saveToken,
    removeToken
}
