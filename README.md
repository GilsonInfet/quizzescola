# Iniciando o projeto Quizz Escolar

Este projeto possui as dependências axios, bootstrap, styled Components,  redux, sweetalert entre outras.
Para rodar localmente você precisa também rodar a contraparte do servidor [https://gitlab.com/GilsonInfet/back_exercicios](https://gitlab.com/GilsonInfet/back_exercicios)

## Scripts Disponíveis

No diretório do projeto, você pode executar:

### `npm start`

Abre no modo de desenvolvimento
Abra [http://localhost:3000](http://localhost:3000) para vê-lo em seu navegador.


A página será recarregada se você fizer edições. \
Você também verá quaisquer erros de lint no console.



### `npm run build`

Compila o aplicativo para produção na pasta `build`. \
Ele agrupa corretamente o React no modo de produção e otimiza a construção para o melhor desempenho.

A compilação reduz os nomes dos arquivos e incluem os hashes. \
Seu aplicativo estará pronto para ser implantado!

### `npm run eject`

** Nota: esta é uma operação unilateral. Depois de `ejetar`, você não pode mais voltar! **

Se você não estiver satisfeito com a ferramenta de construção e as opções de configuração, você pode `ejetar` a qualquer momento. Este comando removerá a dependência única de compilação de seu projeto.

Em vez disso, ele copiará todos os arquivos de configuração e as dependências transitivas (webpack, Babel, ESLint, etc) diretamente para o seu projeto para que você tenha controle total sobre eles. Todos os comandos, exceto `eject`, ainda funcionarão, mas irão apontar para os scripts copiados para que você possa ajustá-los. Neste ponto, você está sozinho.

Você não precisa usar `ejetar`. O conjunto de recursos selecionados é adequado para implantações pequenas e médias, e você não deve se sentir obrigado a usar esse recurso. No entanto, entendemos que esta ferramenta não seria útil se você não pudesse personalizá-la quando estiver pronto para ela.



